O:

This morning, I also conducted a code review of the previous day's homework, and then reviewed and summarized the knowledge learned in the previous week with concept map as a team. In the afternoon, I introduced some basic knowledge of HTTP, RESTful, pair programming and Spring Boot. Then I practiced accordingly.

R:

What I learned today was very interesting. Reviewing the knowledge made me quickly recall what I learned last week and connected the relevant knowledge together. The programming in the afternoon was also very interesting.

I:

Through today's study, I have learned the most basic interface writing, and can successfully test with postman. Pair programming can also see the programming habits of my peers, which is very helpful to my development.

D:

In the future learning process, I will periodically use concept map to summarize my previous learning content and try to link them together to achieve the most efficient learning purpose. I will also study Spring Boot hard after class.