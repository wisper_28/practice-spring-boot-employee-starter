package com.thoughtworks.springbootemployee.controller;

import com.thoughtworks.springbootemployee.entity.Employee;
import com.thoughtworks.springbootemployee.repository.EmployeeRepository;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

@RestController
public class EmployeeController {
    @Resource
    private EmployeeRepository employeeRepository;

    @PostMapping("/employees")
    @ResponseStatus(HttpStatus.CREATED)
    public Employee createEmployee(@RequestBody Employee employee) {
        return employeeRepository.save(employee);
    }

    @GetMapping("/employees")
    public List<Employee> getAllEmployees() {
        return employeeRepository.findAll();
    }

    @GetMapping("/employees/{id}")
    public Employee getEmployeeById(@PathVariable Long id) {
        return employeeRepository.findEmployById(id);
    }

    @GetMapping(value = "/employees", params = {"gender"})
    public List<Employee> findEmployeesByGender(@RequestParam String gender) {
        return employeeRepository.findEmployByGender(gender);
    }

    @PutMapping(value = "/employees/{id}")
    public Employee updateEmployee(@PathVariable Long id, @RequestBody Employee employee) {
        return employeeRepository.updateEmployee(id, employee);
    }

    @DeleteMapping(value = "/employees/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteEmployee(@PathVariable Long id) {
        employeeRepository.deleteEmployById(id);
    }

    @GetMapping(value = "/employees", params = {"page", "size"})
    public List<Employee> getEmployeesByPage(@RequestParam int page, @RequestParam int size) {
        if (size <= 0 || page <= 0) return null;
        return employeeRepository.findEmployeeByPageAndSize(page, size);
    }

}
