package com.thoughtworks.springbootemployee.repository;

import com.thoughtworks.springbootemployee.entity.Company;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;
@Repository
public class CompanyRepository {
    private final List<Company> companyList = new ArrayList<>();

    public List<Company> findAll() {
        return companyList;
    }

    public Company findCompanyById(long id) {
        return companyList.stream()
                .filter(company -> company.getId() == id)
                .findFirst()
                .orElse(null);
    }

    public List<Company> findEmployeeByPageAndSize(int page, int size) {
        return companyList.stream()
                .skip((long) (page - 1) * size)
                .limit(size)
                .collect(Collectors.toList());
    }

    public Company save(Company company) {
        company.setId(generateId());
        companyList.add(company);
        return company;
    }

    private long generateId() {
        return companyList.stream()
                .max(Comparator.comparingLong(Company::getId))
                .map(company -> company.getId() + 1)
                .orElse(1L);
    }

    public Company updateCompany(long id, Company company) {
        return companyList.stream()
                .filter(company1 -> company1.getId() == id)
                .findFirst()
                .map(company1 -> {
                    company1.setName(company.getName());
                    return company1;
                })
                .orElse(null);
    }

    public void deleteCompanyById(long id) {
        companyList.removeIf(company -> company.getId() == id);
    }
}
