package com.thoughtworks.springbootemployee.repository;

import com.thoughtworks.springbootemployee.entity.Employee;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@Repository
public class EmployeeRepository {
    private final List<Employee> employees = new ArrayList<>();

    public Employee save(Employee employee) {
        employee.setId(generateId());
        this.employees.add(employee);
        return employee;
    }

    private Long generateId() {
        return employees.stream()
                .max(Comparator.comparingLong(Employee::getId))
                .map(employee -> employee.getId() + 1)
                .orElse(1L);
    }

    public List<Employee> findAll() {
        return employees;
    }

    public Employee findEmployById(Long id) {
        return employees.stream()
                .filter(employee -> employee.getId().equals(id))
                .findFirst().orElse(null);
    }

    public List<Employee> findEmployByGender(String gender) {
        return employees.stream()
                .filter(employee -> employee.getGender().equals(gender))
                .collect(Collectors.toList());
    }

    public Employee updateEmployee(Long id, Employee employee) {
        Employee findEmployee = employees.stream()
                .filter(employee1 -> employee1.getId().equals(id))
                .findFirst().orElse(null);
        assert findEmployee != null;
        findEmployee.setAge(employee.getAge());
        findEmployee.setSalary(employee.getSalary());
        findEmployee.setCompanyId(employee.getCompanyId());
        return findEmployee;
    }

    public void deleteEmployById(Long id) {
        employees.removeIf(employee -> employee.getId().equals(id));
    }

    public List<Employee> findEmployeeByPageAndSize(int page, int size) {
        return employees.stream()
                .skip((long) (page - 1) * size)
                .limit(size)
                .collect(Collectors.toList());
    }

    public List<Employee> findCompanyEmployees(long id) {
        return employees.stream()
                .filter(employee -> employee.getCompanyId().equals(id))
                .collect(Collectors.toList());
    }

    public void deleteALLCompanyEmployees(long id) {
        employees.removeIf(employee -> employee.getCompanyId().equals(id));
    }

    public void clearAll() {
        employees.clear();
    }
}
