package com.thoughtworks.springbootemployee;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.thoughtworks.springbootemployee.entity.Employee;
import com.thoughtworks.springbootemployee.repository.EmployeeRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import javax.annotation.Resource;

import static org.hamcrest.Matchers.hasSize;
import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
@AutoConfigureMockMvc
public class EmployeeControllerTest {

    @Resource
    private EmployeeRepository employeeRepository;

    @Autowired
    private MockMvc client;

    @BeforeEach
    public void setUp() {
        employeeRepository.clearAll();
    }

    @Test
    void should_create_employee_when_perform_create_employee_given_employee() throws Exception {
        //given
        Employee employee = new Employee(1L, "Lucy", 20, "female", 3000);
        String employeeJson = new ObjectMapper().writeValueAsString(employee);
        //then
        client.perform(MockMvcRequestBuilders.post("/employees")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(employeeJson))
                .andExpect(MockMvcResultMatchers.status().isCreated())
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").isNumber())
                .andExpect(MockMvcResultMatchers.jsonPath("$.name").value("Lucy"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.age").value(20))
                .andExpect(MockMvcResultMatchers.jsonPath("$.gender").value("female"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.salary").value(3000));

        Employee saveEmployee = employeeRepository.findAll().get(0);
        assertEquals("Lucy", saveEmployee.getName());
        assertEquals(20, saveEmployee.getAge());
        assertEquals("female", saveEmployee.getGender());
        assertEquals(3000, saveEmployee.getSalary());
    }

    @Test
    void should_update_employee_when_perform_update_employee_given_employee() throws Exception {
        //given
        Employee employee = new Employee(1L, "Lucy", 20, "female", 3000);
        Employee updateEmployee = new Employee(1L, "Lucy", 2, "female", 30000);
        String employeeJson = new ObjectMapper().writeValueAsString(updateEmployee);
        employeeRepository.save(employee);
        //then
        client.perform(MockMvcRequestBuilders.put("/employees/1")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(employeeJson))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").isNumber())
                .andExpect(MockMvcResultMatchers.jsonPath("$.name").value("Lucy"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.age").value(2))
                .andExpect(MockMvcResultMatchers.jsonPath("$.gender").value("female"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.salary").value(30000));

        Employee saveEmployee = employeeRepository.findAll().get(0);
        assertEquals("Lucy", saveEmployee.getName());
        assertEquals(2, saveEmployee.getAge());
        assertEquals("female", saveEmployee.getGender());
        assertEquals(30000, saveEmployee.getSalary());
    }

    @Test
    void should_get_all_employees_when_perform_get_all_employee_given_employees() throws Exception {
        //given
        Employee employee1 = new Employee(1L, "Lucy", 20, "female", 3000);
        Employee employee2 = new Employee(1L, "Lucy", 21, "female", 3100);
        employeeRepository.save(employee1);
        employeeRepository.save(employee2);
        //then
        client.perform(MockMvcRequestBuilders.get("/employees"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$", hasSize(2)))
                .andExpect(MockMvcResultMatchers.jsonPath("$.[0].id").isNumber());
    }

    @Test
    void should_get_employee_when_perform_get_employee_given_employeeId() throws Exception {
        //given
        Employee employee = new Employee(1L, "Lucy", 20, "female", 3000);
        employeeRepository.save(employee);
        //then
        client.perform(MockMvcRequestBuilders.get("/employees/1"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").isNumber())
                .andExpect(MockMvcResultMatchers.jsonPath("$.name").value("Lucy"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.age").value(20))
                .andExpect(MockMvcResultMatchers.jsonPath("$.gender").value("female"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.salary").value(3000));
    }

    @Test
    void should_delete_employee_when_perform_delete_employee_given_employee() throws Exception {
        //given
        Employee employee = new Employee(1L, "Lucy", 20, "female", 3000);
        employeeRepository.save(employee);
        //then
        client.perform(MockMvcRequestBuilders.delete("/employees/1"))
                .andExpect(MockMvcResultMatchers.status().isNoContent());

    }
}
